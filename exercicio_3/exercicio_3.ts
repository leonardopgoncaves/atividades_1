/*Faça um programa que receba três notas, calcule e mostre a
média ponderada entre elas.
*/

//inicio

namespace exercicio_3
{
    let nota1, nota2, nota3, peso1, peso2, peso3: number;

    nota1 = 2;
    nota2 = 3;
    nota3 = 5;
    peso1 = 5;
    peso2 = 2;
    peso3 = 3;

    let resultado: number;

    resultado = (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 + peso3);

    console.log(`o resultado é: ${resultado}`);
}