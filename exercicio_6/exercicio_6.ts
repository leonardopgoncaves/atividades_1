/*
Faça um programa que receba o salário-base de um
funcionário, calcule e mostre o salário a receber, sabendo-se
que esse funcionário tem gratificação de 5% sobre o salário
base e paga imposto de 7% sobre o salário-base.
*/

namespace exercicio_6
{
    let salario, imposto, aumento: number;

    salario = 1000;
    imposto = 7/100;
    aumento = 5/100;

    let resultado: number;
    let salario_sem_descontar: number;
    let salario_descontado: number;

    salario_sem_descontar = salario + salario * aumento;
    salario_descontado = salario * imposto;
    resultado = salario_sem_descontar - salario_descontado;
    console.log(`o resultado é: ${resultado}`);
}
