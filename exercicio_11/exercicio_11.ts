/*
Faça um programa que receba um número positivo e
maior que zero, calcule e mostre:
a) O número digitado ao quadrado;
b) O número digitado ao cubo;
c) A raiz quadrada do número digitado;
d) A raiz cúbica de número digitado.
*/
namespace exercicio_11
{
    //entrada de dados
    let numero1, numero2, numero3, numero4: number;

    numero1 = 6
    numero2 = 6
    numero3 = 9
    numero4 = 125

    let resultado1: number;
    let resultado2: number;
    let resultado3: number;
    let resultado4: number;

    //prossesar os dados
    resultado1 = numero1 * numero1;

    resultado2 = numero2 * numero2 * numero2;

    resultado3 = Math.sqrt(numero3);

    resultado4 = Math.cbrt(numero4);

    console.log(`resultado do numero elevado ao quadrado é: ${resultado1}`);
    console.log(`resultado do numero elevado ao cubo é: ${resultado2}`);
    console.log(`resultado da raiz quadrada do numero é: ${resultado3}`);
    console.log(`resultado da raiz cubica do numero é: ${resultado4}`);
}