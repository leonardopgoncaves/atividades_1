/*
Faça um programa que receba o valor de um depósito e o
valor da taxa de juros, calcule e mostre o valor do rendimento
e o valor total depois do rendimento.
*/

namespace exercicio_8
{
    let deposito, juros: number;

    deposito = 1000;
    juros = 10/100;

    let rendimento;
    let valor_total;

    rendimento = juros * deposito;
    valor_total = rendimento + deposito;

    console.log(`o resultado é: ${rendimento}`);
    console.log(`o resultado é: ${valor_total}`);
}