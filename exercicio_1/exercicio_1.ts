//comentar
/*
Faça um programa que receba quatro números inteiros,
calcule e mostre a soma desses números.
*/

//primeiro bloco
//inicio
namespace exercicio_1
{
    //Entrada dos dados
    //var --- let --- const
    let numero1, numero2, numero3, numero4: number;

    numero1 = 6;
    numero2 = 7;
    numero3 = 16;
    numero4 = 34;

    let resultado: number;

    //processar os dados
    resultado = numero1 + numero2 + numero3 + numero4;

    //saida
    console.log("O resultado da soma é: " + resultado);
    //ou pode-se escrever
    console.log(`O resultado da soma é: ${resultado}`);
}