/*
Faça um programa que receba o salário-base de um funcionário, calcule e mostre o seu salário a receber, sabendo-
se que esse funcionário tem gratificação de R$50,00 e paga imposto de 10% sobre o salário-base.
*/

namespace exercicio_7
{
    let salario, aumento, imposto: number;

    salario = 1000;
    aumento = 50;
    imposto = 10/100;

    let salario_novo: number;
    let salario_sem_descontar: number;
    let salario_descontado: number;

    salario_sem_descontar = salario + aumento;
    salario_descontado = salario * imposto;
    salario_novo = salario_sem_descontar - salario_descontado;

    console.log(`o resultado é: ${salario_novo}`);
}